from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
def home(request):
    passed = {
        'name' : 'Widy'
    }
    return render(request, 'home.html', passed)

def hitung(request):
    passed = {

    }
    return render(request, 'hitung.html',passed)

def add(request):
    val1 = int(request.POST['num1'])
    val2 = int(request.POST['num2'])
    res = val1 + val2
    passed = {
        'result' : res
    }
    return render(request, 'result.html', passed)