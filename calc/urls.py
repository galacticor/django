from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('hitung', views.hitung, name='hitung'),
    path('add', views.add, name='add')
]