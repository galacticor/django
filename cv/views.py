from django.shortcuts import render
from django.http import HttpResponse
from .models import Skills

# Create your views here.

def index(request):
    skill1 = Skills()
    skill1.name = 'Problem Solving'
    skill1.percent = 90

    skill2 = Skills()
    skill2.name = 'Competitive Programming'
    skill2.percent = 85

    skill3 = Skills()
    skill3.name = 'Python'
    skill3.percent = 90

    skill4 = Skills()
    skill4.name = 'Java'
    skill4.percent = 70

    skill5 = Skills()
    skill5.name = 'Statistic'
    skill5.percent = 80

    skills = [skill1, skill2, skill3, skill4, skill5]
    return render(request, "index.html", {'skills' : skills})